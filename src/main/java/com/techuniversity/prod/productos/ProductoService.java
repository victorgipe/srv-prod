package com.techuniversity.prod.productos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductoService {

    @Autowired
    private ProductoRepositorio productoRepository;

    public ProductoRepositorio getProductoRepository() {
        return productoRepository;
    }

    public void setProductoRepository(ProductoRepositorio productoRepository) {
        this.productoRepository = productoRepository;
    }

    public List<ProductoModel> findAll(){
        return this.getProductoRepository().findAll();
    }

    public Optional<ProductoModel> findById(String id){
        return this.getProductoRepository().findById(id);
    }

    public ProductoModel save (ProductoModel producto){
        return this.getProductoRepository().save(producto);
    }

    public boolean deleteProducto(ProductoModel producto){

        try{
            this.getProductoRepository().delete(producto);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public List<ProductoModel> findPaginado (int page){
        Pageable pageable = PageRequest.of(page, 3);
        Page<ProductoModel> pages = this.getProductoRepository().findAll(pageable);
        List<ProductoModel> productos = pages.getContent();
        return productos;
    }
}