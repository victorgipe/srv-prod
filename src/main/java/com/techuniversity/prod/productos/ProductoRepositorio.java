package com.techuniversity.prod.productos;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductoRepositorio extends MongoRepository<ProductoModel, String> {

}